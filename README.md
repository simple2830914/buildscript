环境初始化
```shell
## docker安装
sudo yum install docker-ce docker-ce-cli containerd.io
# 阿里云安装docker https://help.aliyun.com/zh/ecs/use-cases/deploy-and-use-docker-on-alibaba-cloud-linux-2-instances#471afb3005tua

#启动Docker
sudo systemctl start docker
#设置Docker开机自启
sudo systemctl enable docker
#查看Docker版本
sudo docker version

##配置镜像加速
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://ch0ky5o1.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo docker login --username=17092552042@163.com registry.cn-hangzhou.aliyuncs.com
### 支持编辑volume中的挂载文件
docker run -it --privileged --pid=host --name volume-web "${registry}/${namespace}/volume:$IMG_VERSION" nsenter -t 1 -m -u -n -i -r
### docker 可视化管理工具
docker run \
-p 13306:9000 \
--name dockerweb \
--privileged=true \
--restart unless-stopped \
-v /var/run/docker.sock:/var/run/docker.sock \
-v dockerweb:/data \
-d portainer/portainer-ce:2.19.4
```
访问portainer
```shell
 portainer访问地址 http://localhost:13306
 admin / Admin@123qwe
```

添加docker image 私仓库 ![img.png](md/img_6.png)

按下图箭头顺序上传 [simple.yml](docker%2Fapps%2Fsimple.yml) 部署stack
![img.png](md/img.png)

```shell
simple服务
http://127.0.0.1:8899/api/junit/testTemplate
pma服务用于 mysql在线管理
http://127.0.0.1:8899/phpmyadmin/
 mysql
 root / admin@123
Filebrowser 服务用于修改simple config
http://127.0.0.1:8899/filebrowser/files/
admin/admin

xxljob 定时任务在线管理
http://127.0.0.1:8899/xxl-job-admin/
admin/admin123
```