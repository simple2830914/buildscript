simple {
  scan {
    package: ["com.simple"]
  }
  spring {
    package: ["com.simple"]
  }
  jersey {
    package: ["com.simple"]
    resourceClass: "com.simple.framework.jersey.SpringResourceConfig"
  }
  jetty {
    name: "simple"
    host: "0.0.0.0"
    port: 8899
    port: ${?server.port}
    basePath: "/api",
  }
  redis {
    deployType = "single" // single | sentinel
    host = "redis"
    port = 6379
    password = ""
    username = ""
    database = 0
    pool: {
      maxWait: 10000
      maxIdle: 100
      minIdle: 20
    }
    sentinel {
      master = "master"
      nodes = ["redis:26301", "redis:26302", "redis:26303"]
    }
  }
  template {
    basePath: ${?template.file}
  }
  db {
    package: ["com.simple.**.mapper"]
    druid: {
      "proxyFilters": ["com.simple.framework.data.DruidSqlAudit"]
      "druid.connectionProperties" = "druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000;",
      "druid.filters": "stat,wall,slf4j,config"
      "druid.mysql.usePingMethod": false,
      "druid.asyncInit": false,
      "druid.initialSize": 10,
      "druid.maxActive": 200,
      "druid.minIdle": 10,
      "druid.maxWait": 120000,
      "druid.testOnBorrow": false,
      "druid.testOnReturn": false,
      "druid.testWhileIdle": true,
      "druid.validationQuery": "SELECT 1",
      "druid.timeBetweenEvictionRunsMillis": 60000,
      "druid.minEvictableIdleTimeMillis": 180000,
      "druid.poolPreparedStatements": true,
      "druid.maxPoolPreparedStatementPerConnectionSize": 20,
      "druid.breakAfterAcquireFailure": false,
      "druid.timeBetweenConnectErrorMillis": 300000,
      "druid.logAbandoned": true,
      "druid.removeAbandoned": true,
      "druid.removeAbandonedTimeout": 180,
      "druid.removeAbandonedTimeoutMillis": 180000
    },
    balancing: false,
    master {
      url: "jdbc:mysql://mysql:3306/simple?allowPublicKeyRetrieval=true&autoReconnect=true&connectTimeout=60000&socketTimeout=60000&rewriteBatchedStatements=true"
      username: root
      password: "admin@123",
    }
    slaves = [
      {
        url: "jdbc:mysql://mysql:3306/simple?allowPublicKeyRetrieval=true&autoReconnect=true&connectTimeout=60000&socketTimeout=60000&rewriteBatchedStatements=true"
        username: root
        password: "admin@123",
      }
    ],
    others = [
      {
        url: "jdbc:mysql://mysql:3306/simple?allowPublicKeyRetrieval=true"
        username: root
        password: "admin@123",
      }
    ]
  }
  data {
    gen {
      database: "simple"
      open: true,
      template {
        createDataBase: "mysql.createDataBase",
        createTable: "mysql.createTable",
        queryAllTables: "mysql.queryAllTables",
        queryAllFields: "mysql.queryAllFields",
        queryAllIndexes: "mysql.queryAllIndexes",
        modifyComment: "mysql.modifyComment",
        createIndex: "mysql.createIndex",
        modifyField: "mysql.modifyField",
        addField: "mysql.addField"
      }
      prop {
        primary: "primary key"
        unique: ", unique {} ({})"
        notNull: "not null",
        default: "default {} ",
        //只支持一级查找
        generatedColumn: " as (json_unquote(json_extract(`ext`,_utf8mb4'$.{}')))"
      }
      typeMap {
        "java.lang.String": {
          "type": "varchar({})",
          "length": "200"
        },
        "java.lang.Long": {
          "type": "bigint"
        },
        "java.lang.Integer": {
          "type": "int",
        },
        "java.lang.Double": {
          "type": "decimal({})"
          "length": "18,6"
        },

        "java.lang.Boolean": {
          "type": "bit({})",
          "length": "1"
        },
        "java.util.Date": {
          "type": "timestamp"
        },
        "ext": {
          "type": "json"
        }
      },
      indexes {
        "user" = ["phone", "email"],
        "common_log" = ["trace_id", "type", "schema_name"]
      }
    }
    connector: {
      file: ${?tmp.file}
    }
  }
  job {
    accessToken = ""
    admin {
      addresses = "http://xxljob:8080/xxl-job-admin"
    }
    executor {
      appname: "simple-executor"
      address: ""
      ip: ""
      port: 9999
      logRetentionDays: 30
    }
  }
}