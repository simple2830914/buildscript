## docker安装
sudo yum install docker-ce docker-ce-cli containerd.io
# 阿里云安装docker https://help.aliyun.com/zh/ecs/use-cases/deploy-and-use-docker-on-alibaba-cloud-linux-2-instances#471afb3005tua


#启动Docker
source ../../gradle/registry.sh
source ../../../version.sh

sudo systemctl start docker
#设置Docker开机自启
sudo systemctl enable docker
#查看Docker版本
sudo docker version

##配置镜像加速
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://dso9vy92.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker


docker run -it --privileged --pid=host --name volume-web "${registry}/${namespace}/volume:$IMG_VERSION" nsenter -t 1 -m -u -n -i -r
docker volume create dockerweb
docker run \
-p 13306:9000 \
--name dockerweb \
--privileged=true \
--restart unless-stopped \
-v /var/run/docker.sock:/var/run/docker.sock \
-v dockerweb:/data \
-d portainer/portainer-ce:2.19.4