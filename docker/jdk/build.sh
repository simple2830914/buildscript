#!/usr/bin/env bash
# shellcheck disable=SC2154

imageName=$(basename "$(pwd)")
wget https://repo.huaweicloud.com/java/jdk/11.0.2+9/jdk-11.0.2_linux-x64_bin.tar.gz jdk-11.0.2_linux-x64_bin.tar.gz
docker buildx build -t "${registry}/${namespace}/$imageName:$IMG_VERSION" .
docker push "${registry}/${namespace}/$imageName:$IMG_VERSION"
rm -rf jdk-11.0.2_linux-x64_bin.tar.gz