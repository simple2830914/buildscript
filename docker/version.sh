#!/usr/bin/env bash
# shellcheck disable=SC2155
export version=1.0.0.1
export IMG_VERSION=$(echo "$version" | awk -F '.' '{print $1"."$2"."$3}')