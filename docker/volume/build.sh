#!/usr/bin/env bash
# shellcheck disable=SC2154
imageName=$(basename "$(pwd)")
docker buildx build -t "${registry}/${namespace}/$imageName:$IMG_VERSION" .
docker push "${registry}/${namespace}/$imageName:$IMG_VERSION"