#!/usr/bin/env bash

function docker_build() {
  ARG=$(echo "${CI_BUILD_TAG}" | awk -F '-' '{print $1}')
  VERSION=$(echo "${CI_BUILD_TAG}" | awk -F '-' '{print $2}')
  GROUP_FLAG=$(echo "${CI_BUILD_TAG}" | awk -F '-' '{print $3}')
 # 判断环境
  CONFIG_KEY=${ARG}
  IMG_VERSION=$(echo "${VERSION}" | awk -F '.' '{print $1"."$2"."$3}')
  BUILD_VERSION=$(echo "${VERSION}" | awk -F '.' '{print $1"."$2"."$3"."$4}')
  CONFIG=output/config/"${CONFIG_KEY}"

  echo "CI_BUILD_TAG:${CI_BUILD_TAG}"
  echo "registry:${registry}"
  echo "namespace:${namespace}"
  echo "ARG:${ARG}"
  echo "VERSION:${VERSION}"
  echo "GROUP_FLAG:${GROUP_FLAG}"
  echo "IMG_VERSION:${IMG_VERSION}"
  echo "BUILD_VERSION:${BUILD_VERSION}"
  echo "CONFIG_KEY:${CONFIG_KEY}"
  echo "CONFIG:${CONFIG}"

  mkdir -p "output/settings"

  cp -rf buildscript/file output/settings/file
  cp -rf buildscript/template output/settings/template
  cp -rf buildscript/config/"${CONFIG_KEY}" output/settings/config
  cp -rf orchestration output/settings/orchestration

  cp buildscript/gradle/Dockerfile output/Dockerfile
  sed -i  "s/#ENVIRONMENT/${CONFIG_KEY}/g" output/Dockerfile
  sed -i  "s/#REGISTRY/${registry}/g" output/Dockerfile
  sed -i  "s/#NAMESPACE/${namespace}/g" output/Dockerfile
  sed -i  "s/#IMG_VERSION/${IMG_VERSION}/g" output/Dockerfile

  docker buildx build -t "${registry}/${namespace}/simple-web:${GROUP_FLAG}-${ARG}-${IMG_VERSION}" output
  docker push "${registry}/${namespace}/simple-web:${GROUP_FLAG}-${ARG}-${IMG_VERSION}"
}
