-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 120.26.135.206    Database: simple
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `common_log`
--

DROP TABLE IF EXISTS `common_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `common_log` (
  `trace_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'traceId',
  `type` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '日志类型[SQL, HTTP, BUSINESS, FILE, JOB]',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `schema_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.sql.schemaName'))) VIRTUAL COMMENT '计划执行Schema',
  `business_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.business.businessName'))) VIRTUAL COMMENT '业务名称',
  `business_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.business.businessId'))) VIRTUAL COMMENT '业务ID',
  `business_step` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.business.businessStep'))) VIRTUAL COMMENT '业务Step名称',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  `job_name` varchar(200) COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.job.jobName'))) VIRTUAL COMMENT '任务名称',
  `data_id` varchar(200) COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.job.dataId'))) VIRTUAL COMMENT '任务执行的数据Id',
  `corporation_power_id` varchar(200) COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.job.corporationPowerId'))) VIRTUAL COMMENT '任务执行的功能Id',
  PRIMARY KEY (`id`),
  KEY `trace_id` (`trace_id`),
  KEY `type` (`type`),
  KEY `schema_name` (`schema_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_log`
--

LOCK TABLES `common_log` WRITE;
/*!40000 ALTER TABLE `common_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporation`
--

DROP TABLE IF EXISTS `corporation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corporation` (
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户名称',
  `owner_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户管理员ID',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporation`
--

LOCK TABLES `corporation` WRITE;
/*!40000 ALTER TABLE `corporation` DISABLE KEYS */;
INSERT INTO `corporation` VALUES ('活力天汇','27913489493000192','23176648645000102',20,_binary '\0',_binary '','2024-02-19 08:44:39','2024-02-20 14:14:45',NULL),('飞致云','27913489493000192','26309248276000111',17,_binary '\0',_binary '','2024-02-19 11:12:47','2024-02-20 11:22:04',NULL);
/*!40000 ALTER TABLE `corporation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporation_power`
--

DROP TABLE IF EXISTS `corporation_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corporation_power` (
  `corporation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户ID',
  `power_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '功能ID',
  `owner_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '负责人ID',
  `open_time` timestamp NULL DEFAULT NULL COMMENT '开通时间',
  `expirate_date` timestamp NULL DEFAULT NULL COMMENT '过期时间',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_power_id` (`corporation_id`,`power_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户功能表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporation_power`
--

LOCK TABLES `corporation_power` WRITE;
/*!40000 ALTER TABLE `corporation_power` DISABLE KEYS */;
/*!40000 ALTER TABLE `corporation_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu` (
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '菜单图标',
  `parent_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父级菜单ID',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='菜单信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `message` (
  `corporation_power_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业功能Id',
  `corporation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户ID',
  `trace_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'traceId',
  `source_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源id',
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源类型[WEBHOOK, CALLBACK, MANUAL, JOB]',
  `platform` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '平台标识',
  `action` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '消息动作类型[NONE, ADD, UPDATE, DELETE, ENABLE, DISABLE]',
  `status` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '处理状态[NONE, PROCESSOR, DONE, ERROR, FAIL]',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='消息记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meta` (
  `config_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '元数据配置Id',
  `corporation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业Id',
  `entity_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '元数据名称',
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '元数据类型[BASE, BUSINESS]',
  `trace_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源traceId',
  `data_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据标识',
  `out_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据出口标识',
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '处理状态[NONE, PREPARATION, PROCESSOR, DONE, FAIL]',
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '处理描述',
  `old_md5` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据旧MD5值',
  `md5` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据MD5值',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  KEY `config_id` (`config_id`),
  KEY `entity_name` (`entity_name`),
  KEY `data_id` (`data_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='元数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta`
--

LOCK TABLES `meta` WRITE;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_config`
--

DROP TABLE IF EXISTS `meta_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meta_config` (
  `corporation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业Id',
  `entity_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '元数据名称',
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '元数据类型[BASE, BUSINESS]',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_entity_name` (`corporation_id`,`entity_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='元数据配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_config`
--

LOCK TABLES `meta_config` WRITE;
/*!40000 ALTER TABLE `meta_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `node` (
  `corporation_power_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '企业功能Id',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '节点名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '节点描述',
  `order` int DEFAULT NULL COMMENT '节点排序',
  `async` bit(1) DEFAULT b'0' COMMENT '异步处理标识',
  `custom_response` bit(1) DEFAULT b'0' COMMENT '是否包装返回类',
  `media_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '自定义方法的格式类型',
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '节点对应类型[PROCESSOR, RESPONSE, FORWARDING]',
  `expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '返回值取值表达式',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_power_id_name_remark` (`corporation_power_id`,`name`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='企业功能节点表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node`
--

LOCK TABLES `node` WRITE;
/*!40000 ALTER TABLE `node` DISABLE KEYS */;
/*!40000 ALTER TABLE `node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `power`
--

DROP TABLE IF EXISTS `power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `power` (
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '功能名称',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '功能描述',
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '功能编码',
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '功能类型[NORMAL, DATA]',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_name` (`code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='功能信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `power`
--

LOCK TABLES `power` WRITE;
/*!40000 ALTER TABLE `power` DISABLE KEYS */;
/*!40000 ALTER TABLE `power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户账号',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户密码',
  `nick_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户姓名',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `phone` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.phone'))) VIRTUAL NOT NULL COMMENT '用户手机号',
  `email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin GENERATED ALWAYS AS (json_unquote(json_extract(`ext`,_utf8mb4'$.email'))) VIRTUAL NOT NULL COMMENT '用户邮件地址',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `phone` (`phone`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`name`, `password`, `nick_name`, `id`, `version`, `is_delete`, `active`, `create_time`, `update_time`, `ext`) VALUES ('wangboyun','123123','汪渤云','27913489493000192',0,_binary '\0',_binary '','2024-02-19 06:45:49','2024-02-19 06:45:49','{\"email\": \"\", \"phone\": \"17092552042\"}');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_corporation`
--

DROP TABLE IF EXISTS `user_corporation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_corporation` (
  `user_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `corporation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户ID',
  `id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '数据主键-flexId算法生成',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '数据版本号',
  `is_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除标识',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT '启用标识',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据入库时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间',
  `ext` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_user_id` (`corporation_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户用户关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_corporation`
--

LOCK TABLES `user_corporation` WRITE;
/*!40000 ALTER TABLE `user_corporation` DISABLE KEYS */;
INSERT INTO `user_corporation` VALUES ('27913489493000192','26309248276000111','27929561339000172',0,_binary '\0',_binary '','2024-02-19 11:13:41','2024-02-19 11:13:41',NULL),('27913489493000192','23176648645000102','27991265463000114',0,_binary '\0',_binary '','2024-02-20 04:22:05','2024-02-20 04:22:05',NULL);
/*!40000 ALTER TABLE `user_corporation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-09 12:18:34
